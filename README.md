# Simple Cityscapes UNet semantic segmentation baseline

This is a codebase for training a simple UNet baseline model on a subset of the [Cityscapes dataset](https://www.cityscapes-dataset.com) (MiniCity) as used in the 1st [VIPriors](http://vipriors.github.io) Semantic Segmentation Challenge.

## Datasets

The task to be performed is semantic segmentation. The training and validation data are subsets of the training split of the Cityscapes dataset. The test set is taken from the validation split of the Cityscapes dataset.

## Setup

#### Dataset

Please follow the instructions below to set up the data for this challenge. The tooling provided automatically arranges the data into appropriate training, validation and testing sets of 200, 100 and 200 images, respectively.

1. Download the `gtFine_trainvaltest.zip` (241MB) and `leftImg8bit_trainvaltest.zip` (11GB) files from the [Cityscapes website](https://www.cityscapes-dataset.com/downloads/) (login required);
2. Extract the ZIP files to a directory containing the `gtFine` and `leftImg8bit` folders.
3. Generate the dataset subset by running `python arrange_images.py 'path/to/cityscapes/directory'`. This creates the `minicity` directory containing the small dataset.

Now you are ready to use the data. You can easily load the dataset into PyTorch using the dataset definition in `helpers/minicity.py`. See `baseline.py` for example usage.

#### Training and prediction

You can now run `python baseline.py` to train the baseline model as described below and perform prediction on the validation set. This creates a `baseline_run` directory containing all logs and other files generated during training and a `results` directory with the final predictions on the validation set.

#### Evaluation

The evaluation criteria are the same as used for the [Cityscapes Pixel-Level Semantic Labeling Task](https://www.cityscapes-dataset.com/benchmarks/#scene-labeling-task). The main metric used to rank submissions is the mean Intersection-over-Union (mIoU). Please refer to the [class definitions as described here](https://www.cityscapes-dataset.com/dataset-overview/#class-definitions) to see which classes are included in the evaluation.

Run `python evaluate.py` to evaulate the predictions in your `results` directory. This creates a `results.txt` files containing the metrics used for the challenge leaderboard and a `results.json` file containing a more detailed evaluation.

## Baselines

We provide a PyTorch implementation of [U-Net](https://arxiv.org/abs/1505.04597) with Batch Normalization. To train and evaluate the model on the MiniCity dataset, run `python baseline.py`. Note: the scripts assumes that a GPU is available in your system. Training for 200 epochs takes approximately 2:15 hours on a system with a GeForce GTX 1080 Ti GPU.

The baseline performance on the provided validation set is given in the table below (results may vary):

| Metric | Score |
| ---------------------------- | --------------- |
| IoU Class                    | 0.40            |
| iIoU Class                   | 0.19            |
| IoU Category                 | 0.72            |
| iIou Category                | 0.55            |
| Accuracy                     | 0.78            |
